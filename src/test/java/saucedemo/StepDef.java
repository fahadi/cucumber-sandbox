package saucedemo;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.List;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.rules.ErrorCollector;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;

public class StepDef {

    private String str = "str";

    private List<Integer> numbers = List.of(0,1,2,3,4);

    @Rule
    public ErrorCollector collector = new ErrorCollector();

    @Before
    public void setup() {
        System.out.println("The setup !");
    }

    @After
    public void teardown() {
        Assertions.assertEquals("str", str);
    }

    @Given("I am on the home page")
    public void goToHomePage() {
        System.out.println("GO TO HOME PAGE!");
        privateMethod1();
        privateMethod3();
    }

    @Given("I use params {string} {string} {string} {string} {string} {string}")
    public void iUseParams(String param1, String param2, String param3, String param4, String param5, String param6) {
        System.out.println("I USE PARAMS");
        try {
            String param1value = ParameterService.INSTANCE.getString(param1);
            System.out.println("*" + param1value + "*");
            String param2value = ParameterService.INSTANCE.getString(param2);
            System.out.println("*" + param2value + "*");
            String param3value = ParameterService.INSTANCE.getString(param3);
            System.out.println("*" + param3value + "*");
            String param4value = ParameterService.INSTANCE.getString(param4);
            System.out.println("*" + param4value + "*");
            String param5value = ParameterService.INSTANCE.getString(param5);
            System.out.println("*" + param5value + "*");
            String param6value = ParameterService.INSTANCE.getString(param6);
            System.out.println("*" + param6value + "*");
        } catch (ParameterException e) {
            System.out.println("oopsie");
        }
    }

    private void privateMethod1() {
        System.out.println("private method 1");
        privateMethod2();
    }

    private void privateMethod2() {System.out.println("private method 2");}

    private void privateMethod3() {System.out.println("private method 3");}

    @When("I fill username {string} and password {string} and validate")
    public void fillUsernameAndPassword(String username, String password) {
        System.out.println("Username : " + username + " - password : " + password);
    }

    @Then("The error message {string} appears")
    public void assertErrorMessage(String errorMessage) {
        System.out.println("Error message : " + errorMessage);
    }

    @Then("I fail but still continue")
    public void failButStillContinue() {
        System.out.println("I pass because CUCUMBER");
    }

    @And("I pass again !")
    public void passAgain() {
        System.out.println("Hello I pass !");
    }

    @And("I use a datatable")
    public void useDatatable(final DataTable table) {
        System.out.println("Datatable : " + table);
    }

    @And("I also use a docstring")
    public void useDocstring(final String docstring) {
        System.out.println("Docstring : " + docstring);
    }

    @And("I pass the first time then fail")
    public void passTheFirstTimeThenFail() {
        Assertions.assertEquals("str", str);
        str = "strr";
    }

    @And("So I am not run")
    public void soIAmNotRun() {
        System.out.println("So I am not run");
    }

    @And("This keyword is {string}")
    public void thisKeywordIs(String keyword) {
        switch (keyword) {
            case "passed":
                System.out.println("I pass!");
                break;
            case "failed":
                Assertions.fail("I fail !");
                break;
        }
    }

    @And("This keyword should be {string}")
    public void thisKeywordShouldBe(String keyword) {
        if (keyword.equals("passed")) {
            System.out.println("If i am not pass this is wrong");
        }
    }

    @When("I run")
    public void run() {
        System.out.println("I run");
    }

    @Then("I pass")
    public void pass() {
        Assertions.assertTrue(true);
        System.out.println("I PASS !");
    }

    @Then("I fail")
    public void fail() {
        Assertions.fail("I fail");
    }
}