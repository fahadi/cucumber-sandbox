package saucedemo;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

public class StepDefPrestashopStyle {

    @Before
    public void setup() {
        System.out.println("The setup !");
    }

    @After
    public void teardown() {
        Assertions.assertEquals("str", "str");
    }

    @Given("I created an account with gender {string} firstName {string} lastName {string} email {string} " +
                   "password {string} birthDate {string} partnerOffers {string} newsletter {string}")
    public void setNewAccount(final String gender,
                              final String firstName,
                              final String lastName,
                              final String email,
                              final String password,
                              final String birthDate,
                              final String partnerOffers,
                              final String newsletter) {

        System.out.println("The new account is created : " + gender + " " + firstName + " " + lastName + " " + email + " " + password + " " + birthDate + " " + partnerOffers + " " + newsletter);
    }

    @Given("I am logged out")
    public void setLogOut() {
        System.out.println("The user is logged out");
    }

    @Given("I am on the {string} page")
    public void setCurrentPage(final String string) {
        System.out.println("The current page is : " + string);
    }

    @When("I navigate to category {string}")
    public void navigateToCategory(final String category) {
        System.out.println("The user is on category : " + category);
    }

    @When("I navigate to product {string}")
    public void navigateToProduct(final String product) {
        System.out.println("The user is on product : " + product);
    }

    @When("I add to cart")
    public void addToCart() {
        System.out.println("The user add to cart");
    }

    @When("I navigate to the {string} page")
    public void navigateToPage(final String string) {
        System.out.println("The user is on page : " + string);
    }

    @When("I initiate order placement process")
    public void initiateOrderProcess() {
        System.out.println("The user initiate order process");
    }

    @When("I fill login form with email {string} and password {string}")
    public void fillLoginForm(final String email,
                              final String password) {
        System.out.println("The user fill login form : " + email + " " + password);
    }

    @When("I fill command form with alias {string} company {string} vat {int} address {string} supp {string} zip {int} city {string} country {string} phone {int} and facturation {string} and submit")
    public void fillCommandForm(final String alias,
                                final String company,
                                final int vat,
                                final String address,
                                final String supp,
                                final int zip,
                                final String city,
                                final String country,
                                final int phone,
                                final String facturation) {
        System.out.println("The user fill command form : " + alias + " " + company + " " + vat + " " + address + " " + supp + " " + zip + " " + city + " " + country + " " + phone + " " + facturation);
    }

    @When("I choose delivery {string} and command message {string}")
    public void fillDeliveryAndCommandMessage(final String delivery,
                                              final String deliveryMessage) {
        System.out.println("The user choose delivery : " + delivery + " " + deliveryMessage);
    }

    @When("I pay by paymode {string} and choose approveSalesConditions {string}")
    public void selectPaymodeAndApproveSalesConditions(final String paymode,
                                                       final String approveSalesConditions) {
        System.out.println("The user choose paymode : " + paymode + " " + approveSalesConditions);
    }

    @When("I submit order")
    public void submitOrder() {
        System.out.println("The user submit order");
    }

    @Then("The order should be placed and it should contain")
    public void assertOrderContent(final DataTable order) {
        System.out.println("The order should be placed and it should contain : " + order);
    }

    @Then("The total order price should be {word}")
    public void assertTotalOrderPrice(final String priceString) {
        System.out.println("The total order price should be : " + priceString);
    }

}