# language: en
Feature: SKIPPED Sauce Demo Test

	Scenario Outline: SKIPPED Sauce Demo Test
		Given I am on the home page
		When I fill username <username> and password <password> and validate
		Then The error message "Epic sadface: Username and password do not match any user in this service" appears
		And This keyword is <kw_status>
		And This keyword should be <kw_status2>

		@SKIP
		Examples:
		| kw_status | kw_status2 | password | username |
		| "failed" | "skipped" | "Croquettes1234" | "gnocchi" |

		@SKIP
		Examples:
		| kw_status | kw_status2 | password | username |
		| "passed" | "passed" | "Croquettes1234" | "gnocchi" |