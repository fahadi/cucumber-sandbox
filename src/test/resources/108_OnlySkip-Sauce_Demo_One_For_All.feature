# language: en
Feature: OnlySkip-Sauce Demo One For All

	Scenario Outline: OnlySkip-Sauce Demo One For All
		Given I am on the home page
		When I fill username <username> and password <password> and validate
		Then The error message "Epic sadface: Username and password do not match any user in this service" appears
		And This keyword is <kw_status>

		@FAIL
		Examples:
		| kw_status | kw_status2 | password | username |
		| "FAIL" | "SKIP" | "Croquettes1234" | "gnocchi" |

		@PASS
		Examples:
		| kw_status | kw_status2 | password | username |
		| "PASS" | "PASS" | "Croquettes1234" | "gnocchi" |

		@SKIP
		Examples:
		| kw_status | kw_status2 | password | username |
		| "SKIP" | "SKIP" | "Croquettes1234" | "gnocchi" |