# language: en
Feature: Should Not Report Step Statuses

	Scenario Outline: Should Not Report Step Statuses
		Given I am on the home page
		When I fill username <username> and password <password> and validate
		Then The error message "Epic sadface: Username and password do not match any user in this service" appears
		And This keyword is <kw_status>
		And This keyword is <kw_status>
		And This keyword should be <kw_status2>

		@FAIL
		Examples:
		| kw_status | kw_status2 | password | username |
		| "failed" | "skipped" | "Croquettes1234" | "gnocchi" |

		@PASS
		Examples:
		| kw_status | kw_status2 | password | username |
		| "passed" | "passed" | "Croquettes1234" | "gnocchi" |