# language: en
Feature: Cucumber Sauce Demo Test

	Scenario Outline: Cucumber Sauce Demo Test
		Given I am on the home page
		When I fill username <username> and password <password> and validate
		Then The error message "Epic sadface: Username and password do not match any user in this service" appears
		Then I fail but still continue
		And I pass again !
		And I use a datatable
			| produit | prix |
			| Expresso | 0.40 |
		And I also use a docstring
			"""
			the docstring
			"""
		And I pass the first time then fail
		And I pass the first time then fail
		And So I am not run

		@Gnocchi
		Examples:
		| password | username |
		| "Croquettes1234" | "gnocchi" |